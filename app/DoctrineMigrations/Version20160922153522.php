<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160922153522 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact CHANGE email email VARCHAR(10) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE contact_time contact_time VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_offer ADD contact_time VARCHAR(20) DEFAULT NULL, ADD content LONGTEXT DEFAULT NULL, DROP firma, DROP adress, DROP fax, DROP contact_info, DROP localization, DROP purpose, DROP surface, DROP width, DROP lenght, DROP height, DROP surface_soc, DROP gate_count, DROP offer_info, CHANGE name name VARCHAR(255) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE email email VARCHAR(10) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE contact_time contact_time VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE contact_offer ADD firma VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD adress VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD fax VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD contact_info VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD localization VARCHAR(255) NOT NULL COLLATE utf8_general_ci, ADD purpose VARCHAR(255) NOT NULL COLLATE utf8_general_ci, ADD surface INT DEFAULT NULL, ADD width INT DEFAULT NULL, ADD lenght INT DEFAULT NULL, ADD height INT DEFAULT NULL, ADD surface_soc INT DEFAULT NULL, ADD gate_count VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD offer_info VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, DROP contact_time, DROP content, CHANGE name name VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE phone phone VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE email email VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
    }
}
