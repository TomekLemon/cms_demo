<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160923092136 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact CHANGE email email VARCHAR(10) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE contact_time contact_time VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_offer ADD name VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, DROP offer_name, DROP offer_phone, DROP offer_email');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE contact_time contact_time VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE contact_offer ADD offer_name VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD offer_phone VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, ADD offer_email VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, DROP name, DROP phone, DROP email');
    }
}
