<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161017163316 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider_photo DROP FOREIGN KEY FK_9203C87C2CCC9638');
        $this->addSql('DROP TABLE slider');
        $this->addSql('DROP INDEX IDX_9203C87C2CCC9638 ON slider_photo');
        $this->addSql('ALTER TABLE slider_photo DROP slider_id, CHANGE title title VARCHAR(255) NOT NULL, CHANGE link link VARCHAR(255) NOT NULL, CHANGE title_img title_img VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE banners CHANGE photo photo VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE realizations CHANGE arrangement arrangement INT NOT NULL, CHANGE type type VARCHAR(255) NOT NULL, CHANGE industry industry VARCHAR(255) NOT NULL, CHANGE province province VARCHAR(255) NOT NULL, CHANGE city city VARCHAR(255) NOT NULL, CHANGE size_param size_param VARCHAR(255) NOT NULL, CHANGE latitude latitude VARCHAR(25) NOT NULL, CHANGE longitude longitude VARCHAR(25) NOT NULL, CHANGE marker_type marker_type VARCHAR(25) DEFAULT \'1\' NOT NULL, CHANGE content content LONGTEXT DEFAULT NULL, CHANGE photo photo VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE realizations ADD CONSTRAINT FK_69C402BE4E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id)');
        $this->addSql('ALTER TABLE realizations RENAME INDEX fk_1gg399504e7af8g TO IDX_69C402BE4E7AF8F');
        $this->addSql('ALTER TABLE realizations_options CHANGE realization_id realization_id INT NOT NULL, CHANGE label label VARCHAR(255) NOT NULL, CHANGE value value VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE realizations_options ADD CONSTRAINT FK_EEC587D81A26530A FOREIGN KEY (realization_id) REFERENCES realizations (id)');
        $this->addSql('ALTER TABLE realizations_options RENAME INDEX fk_9203c87c2gcc9638 TO IDX_EEC587D81A26530A');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E4E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id)');
        $this->addSql('CREATE INDEX IDX_29D6873E4E7AF8F ON offer (gallery_id)');
        $this->addSql('DROP INDEX FK_F02B543B4E7AF8F ON offer_examples');
        $this->addSql('DROP INDEX FK_F02A543F4E7AF8F ON offer_examples');
        $this->addSql('ALTER TABLE partners CHANGE link link VARCHAR(255) NOT NULL, CHANGE photo photo VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slider (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(191) NOT NULL COLLATE utf8mb4_unicode_ci, photo_width INT NOT NULL, photo_height INT NOT NULL, UNIQUE INDEX UNIQ_CFC71007EA750E8 (label), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE banners CHANGE photo photo VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E4E7AF8F');
        $this->addSql('DROP INDEX IDX_29D6873E4E7AF8F ON offer');
        $this->addSql('CREATE INDEX FK_F02B543B4E7AF8F ON offer_examples (offer_id)');
        $this->addSql('CREATE INDEX FK_F02A543F4E7AF8F ON offer_examples (realization_id)');
        $this->addSql('ALTER TABLE partners CHANGE link link VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, CHANGE photo photo VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE realizations DROP FOREIGN KEY FK_69C402BE4E7AF8F');
        $this->addSql('ALTER TABLE realizations CHANGE arrangement arrangement INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE industry industry VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE province province VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE city city VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE size_param size_param VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE latitude latitude VARCHAR(25) DEFAULT NULL COLLATE utf8_general_ci, CHANGE longitude longitude VARCHAR(25) DEFAULT NULL COLLATE utf8_general_ci, CHANGE marker_type marker_type INT DEFAULT 1, CHANGE content content LONGTEXT NOT NULL COLLATE utf8_general_ci, CHANGE photo photo VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE realizations RENAME INDEX idx_69c402be4e7af8f TO FK_1GG399504E7AF8G');
        $this->addSql('ALTER TABLE realizations_options DROP FOREIGN KEY FK_EEC587D81A26530A');
        $this->addSql('ALTER TABLE realizations_options CHANGE realization_id realization_id INT DEFAULT NULL, CHANGE label label VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, CHANGE value value VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE realizations_options RENAME INDEX idx_eec587d81a26530a TO FK_9203C87C2GCC9638');
        $this->addSql('ALTER TABLE slider_photo ADD slider_id INT DEFAULT NULL, CHANGE title title VARCHAR(255) DEFAULT \'Tytuł\' COLLATE utf8mb4_unicode_ci, CHANGE title_img title_img VARCHAR(255) DEFAULT \'Tytuł zdjęcia\' COLLATE utf8mb4_unicode_ci, CHANGE link link VARCHAR(255) DEFAULT \'#\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE slider_photo ADD CONSTRAINT FK_9203C87C2CCC9638 FOREIGN KEY (slider_id) REFERENCES slider (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9203C87C2CCC9638 ON slider_photo (slider_id)');
    }
}
