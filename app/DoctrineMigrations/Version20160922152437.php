<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160922152437 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact_offer');
        $this->addSql('ALTER TABLE contact CHANGE email email VARCHAR(10) DEFAULT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL, CHANGE contact_time contact_time VARCHAR(20) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact_offer (id INT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_general_ci, firm VARCHAR(255) NOT NULL COLLATE utf8_general_ci, phone VARCHAR(255) NOT NULL COLLATE utf8_general_ci, adress VARCHAR(255) NOT NULL COLLATE utf8_general_ci, fax VARCHAR(255) NOT NULL COLLATE utf8_general_ci, email VARCHAR(255) NOT NULL COLLATE utf8_general_ci, contact_info VARCHAR(255) NOT NULL COLLATE utf8_general_ci, localization VARCHAR(255) NOT NULL COLLATE utf8_general_ci, purpose VARCHAR(255) NOT NULL COLLATE utf8_general_ci, surface INT NOT NULL, width INT NOT NULL, lenght INT NOT NULL, height INT NOT NULL, surface_soc INT NOT NULL, gate_count VARCHAR(255) NOT NULL COLLATE utf8_general_ci, offer_info VARCHAR(255) NOT NULL COLLATE utf8_general_ci, created_at DATETIME NOT NULL) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact CHANGE phone phone VARCHAR(20) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE contact_time contact_time VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
