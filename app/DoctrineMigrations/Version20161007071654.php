<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161007071654 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE banners CHANGE photo photo VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE realizations CHANGE arrangement arrangement INT NOT NULL, CHANGE type type VARCHAR(255) NOT NULL, CHANGE industry industry VARCHAR(255) NOT NULL, CHANGE province province VARCHAR(255) NOT NULL, CHANGE city city VARCHAR(255) NOT NULL, CHANGE size_param size_param VARCHAR(255) NOT NULL, CHANGE latitude latitude VARCHAR(25) NOT NULL, CHANGE longitude longitude VARCHAR(25) NOT NULL, CHANGE marker_type marker_type VARCHAR(25) DEFAULT \'1\' NOT NULL, CHANGE content content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE realizations ADD CONSTRAINT FK_69C402BE4E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id)');
        $this->addSql('ALTER TABLE realizations RENAME INDEX fk_1gg399504e7af8g TO IDX_69C402BE4E7AF8F');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE banners CHANGE photo photo VARCHAR(255) NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE realizations DROP FOREIGN KEY FK_69C402BE4E7AF8F');
        $this->addSql('ALTER TABLE realizations CHANGE arrangement arrangement INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE industry industry VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE province province VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE city city VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE size_param size_param VARCHAR(255) DEFAULT \'-\' COLLATE utf8_general_ci, CHANGE latitude latitude VARCHAR(25) DEFAULT NULL COLLATE utf8_general_ci, CHANGE longitude longitude VARCHAR(25) DEFAULT NULL COLLATE utf8_general_ci, CHANGE marker_type marker_type INT DEFAULT 1, CHANGE content content LONGTEXT NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE realizations RENAME INDEX idx_69c402be4e7af8f TO FK_1GG399504E7AF8G');
    }
}
