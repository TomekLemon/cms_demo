<?php

namespace Ls\RealizationsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\RealizationsBundle\Entity\Realizations;
use Ls\RealizationsBundle\Entity\RealizationsOptions;


class RealizationsUpdater implements EventSubscriber {

    protected $realization_id;

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Realizations) {

            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }

             if ($entity->getMarkerType() == null) {
                $entity->setMarkerType(1);
             }

             if ($entity->getType() == null) {
                $entity->setType('-');
             }

             if ($entity->getIndustry() == null) {
                $entity->setIndustry('-');
             }

             if ($entity->getProvince() == null) {
                $entity->setProvince('-');
             }

             if ($entity->getCity() == null) {
                $entity->setCity('-');
             }

             if ($entity->getSizeParam() == null) {
                $entity->setSizeParam('-');
             }
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Realizations) {

            $this->realization_id = $entity->getId();

            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsMenuBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.route_parameters LIKE :slug')
                        ->setParameter('route', 'ls_realizations_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
        if ($entity instanceof RealizationsOptions) {
            $entity->setRealizationId($this->realization_id);
            $em->persist($entity);
            $em->flush();
        }
    }
    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Realizations) {
            $entity->setUpdatedAt(new \DateTime());
            
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Realizations) {
            $this->realization_id = $entity->getId();
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMenuBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.route_parameters LIKE :slug')
                    ->setParameter('route', 'ls_realizations_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
        }

        if ($entity instanceof RealizationsOptions) {
            $entity->setRealizationId($this->realization_id);
            $em->persist($entity);
            $em->flush();
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Realizations) {
            $entity->deletePhoto();
        }
    }

}