<?php

namespace Ls\RealizationsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ls\RealizationsBundle\Entity\RealizationsOptions;

/**
 * RealizationsOptions
 * @ORM\Table(name="realizations_options")
 * @ORM\Entity
 */
class RealizationsOptions {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @var Realizations
     * 
     * @ORM\ManyToOne(targetEntity="Realizations", inversedBy="options", cascade={"persist"})
     * @ORM\JoinColumn(name="realization_id", referencedColumnName="id")
     */
    private $realization;


    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="integer", length=11)
     * @var integer
     */
    private $realization_id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $value;


    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return RealizationsOptions
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return RealizationsOptions
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set realization_id
     *
     * @param integer $realization_id
     * @return RealizationsOptions
     */
    public function setRealizationId($realization_id)
    {
        $this->realization_id = $realization_id;

        return $this;
    }

    /**
     * Get realization_id
     *
     * @return integer
     */
    public function getRealizationId()
    {
        return $this->realization_id;
    }
}