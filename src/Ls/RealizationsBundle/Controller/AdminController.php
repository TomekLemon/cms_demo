<?php

namespace Ls\RealizationsBundle\Controller;

use Ls\RealizationsBundle\Entity\Realizations;
use Ls\RealizationsBundle\Entity\RealizationsOptions;
use Ls\RealizationsBundle\Form\RealizationsType;
use Ls\RealizationsBundle\Form\RealizationsOptionsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_realizations_pager_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e', 'g')
            ->from('LsRealizationsBundle:Realizations', 'e')
            ->leftJoin('e.gallery', 'g')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.published_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_realizations'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Realizacje', $this->get('router')->generate('ls_admin_realizations'));

        return $this->render('LsRealizationsBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Realizations();
        $size = $entity->getThumbSize('list');
        
        $form = $this->createForm(RealizationsType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_realizations_new'),
            'method' => 'POST'
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $sFileName = uniqid('project-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);

            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie realizacji zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_realizations_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_realizations'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_realizations_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Realizacje', $this->get('router')->generate('ls_admin_realizations'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_realizations_new'));

        return $this->render('LsRealizationsBundle:Admin:new.html.twig', array(
            'form' => $form->createView(),
            'size' => $size,
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsRealizationsBundle:Realizations')->find($id);
        $size = $entity->getThumbSize('list');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Realizations entity.');
        }

        $form = $this->createForm(RealizationsType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_realizations_edit', array('id' => $entity->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $entity->deletePhoto();

                $sFileName = uniqid('project-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja realizacji zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_realizations_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_realizations'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Realizacje', $this->get('router')->generate('ls_admin_realizations'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_realizations_edit', array('id' => $entity->getId())));

        return $this->render('LsRealizationsBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'id' => $id,
            'size' => $size,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsRealizationsBundle:Realizations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Realizations entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls_core.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie realizacji zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Realizacje', $this->get('router')->generate('ls_admin_realizations'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_realizations_batch'));

            return $this->render('LsRealizationsBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_realizations'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsRealizationsBundle:Realizations', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    $sitemap = $this->get('ls_core.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_realizations'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_realizations'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }

    public function kadrujAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');

        $entity = $em->getRepository('LsRealizationsBundle:Realizations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Realizations entity.');
        }

        if (null === $entity->getPhotoAbsolutePath()) {
            return $this->redirect($this->generateUrl('ls_admin_realizations'));
        } else {
            $size = $entity->getThumbSize($type);
            $photo = $entity->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Oferta', $this->get('router')->generate('ls_admin_realizations'));
            $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_realizations_edit', array('id' => $entity->getId())));
            $breadcrumbs->addItem('Kadrowanie', $this->get('router')->generate('ls_admin_realizations_crop', array('id' => $entity->getId(), 'type' => $type)));

            return $this->render('LsRealizationsBundle:Admin:kadruj.html.twig', array(
                'entity' => $entity,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
            ));
        }
    }

    public function kadrujZapiszAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');
        $x = $request->get('x');
        $y = $request->get('y');
        $x2 = $request->get('x2');
        $y2 = $request->get('y2');

        $entity = $em->getRepository('LsRealizationsBundle:Realizations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Realizations entity.');
        }

        $entity->Thumb($x, $y, $x2, $y2, $type);

        $this->get('session')->getFlashBag()->add('success', 'Kadrowanie miniatury zakończone sukcesem.');

        return $this->redirect($this->generateUrl('ls_admin_realizations_edit', array('id' => $entity->getId())));
    }
}
