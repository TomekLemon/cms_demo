<?php

namespace Ls\RealizationsBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Realizations controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Realizations entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_realizations'));
        }

        if ($session->has('realizations_filter_status')) {
            $filter_status = $session->get('realizations_filter_status');
        } else {
            $filter_status = 0;
            $session->set('realizations_filter_status', $filter_status);
        }

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_realizations')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsRealizationsBundle:Realizations', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $count = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsRealizationsBundle:Realizations', 'c')
            ->where($qb->expr()->isNotNull('c.published_at'))
            ->andWhere('c.published_at <= :today')
            ->orderBy('c.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getSingleScalarResult();

        $city_count_query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsRealizationsBundle:Realizations', 'c')
            ->where($qb->expr()->isNotNull('c.published_at'))
            ->andWhere('c.published_at <= :today')
            ->groupBy('c.city')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        $on_map = $em->createQueryBuilder()
            ->select('m')
            ->from('LsRealizationsBundle:Realizations', 'm')
            ->where($qb->expr()->isNotNull('m.published_at'))
            ->andWhere('m.published_at <= :today')
            ->orderBy('m.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();
            
        $city_count = 0;
        foreach ($city_count_query as $cities) {
            $city_count++;
        }

        if (isset($_POST["index_city"]))
            $index_city = $_POST["index_city"];
        else
            $index_city = "null";

        return $this->render('LsRealizationsBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'count' => $count,
            'city_count' => $city_count,
            'on_map' => $on_map,
            'allow' => $allow,
            "index_city" => $index_city,
        ));
    }

    /**
     * Finds and displays a Realizations entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsRealizationsBundle:Realizations', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        $options = $em->createQueryBuilder()
            ->select('r')
            ->from('LsRealizationsBundle:RealizationsOptions', 'r')
            ->where('r.realization_id = :realization_id')
            ->setParameter('realization_id', $entity->getId())
            ->getQuery()
            ->getResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Realizations entity.');
        }

        return $this->render('LsRealizationsBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'options' => $options,
        ));
    }

    public function ajaxMoreAction(Request $request, $title = 'null', $type = 'null', $industry = 'null', $province = 'null', $city = 'null') {
        $em = $this->getDoctrine()->getManager();

        $session = $this->container->get('session');

        if ($session->has('realizations_filter_status')) {
            $filter_status = $session->get('project_filter_status');
        } else {
            $filter_status = 0;
            $session->set('realizations_filter_status', $filter_status);
        }

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_realizations')->getValue();
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
            $qb->select('a');
            $qb->from('LsRealizationsBundle:Realizations', 'a');
            $qb->where($qb->expr()->isNotNull('a.published_at'));
            $qb->andWhere('a.published_at <= :today');
                if ($title != 'null') {
                    $qb->andWhere('a.title LIKE :title');
                    $qb->setParameter(':title', $title);
                }
                if ($type != 'null') {
                    $qb->andWhere('a.type LIKE :type');
                    $qb->setParameter(':type', $type);
                }
                if ($industry != 'null') {
                    $qb->andWhere('a.industry LIKE :industry');
                    $qb->setParameter(':industry', $industry);
                }
                if ($province != 'null') {
                    $qb->andWhere('a.province LIKE :province');
                    $qb->setParameter(':province', $province);
                }
                if ($city != 'null') {
                    $qb->andWhere('a.city LIKE :city');
                    $qb->setParameter(':city', $city);
                }
            $qb->orderBy('a.published_at', 'DESC');
            $qb->setParameter('today', $today, Type::DATETIME);
            $qb->setFirstResult($start);
            $qb->setMaxResults($limit);
            $query = $qb->getQuery();
            $entities = $query->getResult();

        if (count($entities) < $limit ) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsRealizationsBundle:Front:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

        public function blockAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_realizations_block')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsRealizationsBundle:Realizations', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsRealizationsBundle:Front:block.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }
}
