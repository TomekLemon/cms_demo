<?php

namespace Ls\RealizationsBundle\Form;

use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;



class RealizationsOptionsType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('label', TextType::class, array(
            'label' => false,
            'attr' => array (
                'placeholder' => 'Tytuł',
                'class' => 'prototype-tytul',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Tytuł. Wypełnij pole'
                ))
            )
        ));

        $builder->add('value', TextType::class, array(
            'label' => false,
            'attr' => array (
                'placeholder' => 'Wartość',
                'class' => 'prototype-wartosc',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wartość. Wypełnij pole'
                ))
            )
        )); 
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\RealizationsBundle\Entity\RealizationsOptions',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_realizations';
    }
}