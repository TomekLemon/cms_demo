<?php

namespace Ls\RealizationsBundle\Form;

use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Ls\GalleryBundle\Form\HasGalleryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Ls\RealizationsBundle\Form\RealizationsOptionsType;


class RealizationsType extends HasGalleryType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('title', null, array(
            'label' => 'Nazwa',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Nazwa. Wypełnij pole'
                ))
            )
        ));

        $builder->add('type', null, array(
            'label' => 'Rodzaj'
        ));

        $builder->add('industry', null, array(
            'label' => 'Branża'
        ));

        $builder->add('province', null, array(
            'label' => 'Województwo'
        ));
        
        $builder->add('city', null, array(
            'label' => 'Miejscowość'
        ));
        
        $builder->add('sizeparam', null, array(
            'label' => 'Wymiary'
        ));


        $builder->add('latitude', null, array(
            'label' => 'Szerokość geogr.'
        ));

        $builder->add('longitude', null, array(
            'label' => 'Długość geogr.'
        ));

        $builder->add('markertype', ChoiceType::class, array(
            'label' => 'Ikona na mapie',
            'choices'  => array(
                '1' => 1,
                '2' => 2,
                '3' => 3,
            ),
            'multiple' => false,
            'expanded' => true,
        ));

        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        $builder->add('published_at', TextType::class, array(
            'label' => 'Data publikacji',
            'attr' => array(
                'data-date-format' => 'DD.MM.YYYY HH:mm'
            )
        ));
        $builder->get('published_at')->addModelTransformer(new DateTimeTransformer());

        $builder->add('content', null, array(
            'label' => 'Treść',
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));


        $builder->add('options', CollectionType::class, array(
            'label'        => 'Dodatkowe wartości',
            'entry_type'   => RealizationsOptionsType::class,
            'allow_add'    => true,
            'allow_delete' => true,
            'prototype'    => true,
            'required'     => false,
            'by_reference' => true,
            'delete_empty' => true,
        ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('list');

            if (!$object || null === $object->getId()) {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            } else {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\RealizationsBundle\Entity\Realizations',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_realizations';
    }
}
