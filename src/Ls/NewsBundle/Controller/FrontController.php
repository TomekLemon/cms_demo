<?php

namespace Ls\NewsBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * News controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all News entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        foreach ($query as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        if (count($query) < $limit) {
            $allow = 0;
        }

        $paginator = $this->get('knp_paginator');
        $page = $request->query->get('page', 1);
        $entities = $paginator->paginate($query, $page, $limit);
        return $this->render('LsNewsBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }

    /**
     * Finds and displays a News entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsNewsBundle:News', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news_block')->getValue();
        $today = new \DateTime();
        
        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($em->createQueryBuilder()->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.slug <> :slug')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('slug', $slug)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return $this->render('LsNewsBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'entities' => $entities,
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        foreach ($entities as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsFormBundle:News:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

        public function blockAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news_block')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsNewsBundle:Front:block.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }
}
