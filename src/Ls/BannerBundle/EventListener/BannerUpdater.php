<?php

namespace Ls\BannerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\BannerBundle\Entity\Banner;

class BannerUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postRemove',
        );
    }


    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Banner) {
            $entity->deletePhoto();
        }
    }

}