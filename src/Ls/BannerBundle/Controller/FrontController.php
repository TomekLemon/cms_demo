<?php

namespace Ls\BannerBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Banner controller.
 *
 */
class FrontController extends Controller {

    public function blockAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsBannerBundle:Banner', 'a')
            ->getQuery()
            ->getResult();

        return $this->render('LsBannerBundle:Front:block.html.twig', array(
            'entities' => $entities,
        ));
    }
}
