<?php

namespace Ls\ContactBundle\Form;

use Ls\CoreBundle\Form\GoogleRecaptchaType;
use Ls\CoreBundle\Validator\Constraints\GoogleRecaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
class ContactType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Twoje imię i nazwisko:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('email', null, array(
                'label' => 'Twój adres e-mail:',
            )
        );

        $builder->add('phone', null, array(
                'label' => 'Twój numer telefonu:',
            )
        );

        $builder->add('contacttime', ChoiceType::class, array(
                'label' => 'Preferowane godziny kontaktu:',
                'choices' => array(
                    '8:00 - 12:00' => '8:00 - 12:00',
                    '12:00 - 16:00' => '12:00 - 16:00',
                    '16:00 - 20:00' => '16:00 - 20:00',
                    'po 20' => 'po 20',
                ),
                'placeholder' => 'Wybierz',
                'empty_data'  => null
            )
        );

        $builder->add('content', TextareaType::class, array(
                'label' => 'Treść wiadomości:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
//        $builder->add('google_recaptcha', GoogleRecaptchaType::class, array(
//                'label' => 'Ochrona antyspamowa',
//                'error_bubbling' => false,
//                'mapped' => false,
//                'constraints' => array(
//                    new GoogleRecaptcha()
//                )
//            )
//        );
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij wiadomość'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact';
    }
}
