<?php

namespace Ls\ContactBundle\Form;

use Ls\CoreBundle\Form\GoogleRecaptchaType;
use Ls\CoreBundle\Validator\Constraints\GoogleRecaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ContactOfferType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('offername', null, array(
                'label' => 'Imię i nazwisko:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('firma', null, array(
                'label' => 'Firma:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('offerphone', null, array(
                'label' => 'Telefon:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('adress', null, array(
                'label' => 'Adres:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('fax', null, array(
                'label' => 'Fax:',
            )
        );

        $builder->add('offeremail', null, array(
                'label' => 'E-mail:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                    new Email(array(
                        'message' => 'Podaj poprawny adres e-mail'
                    ))
                )
            )
        );

        $builder->add('contact_info', null, array(
                'label' => 'Informacje dodatkowe:',
            )
        );

        $builder->add('localization', null, array(
                'label' => 'Planowana lokalizacja:',
            )
        );


        $builder->add('purpose', ChoiceType::class, array(
                'label' => 'Przeznaczenie obiektu:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wybierz odpowiedź'))
                ),
                'choices' => array(
                    'Hala produkcyjna' => 'Hala produkcyjna',
                    'Hala magazynowa' => 'Hala magazynowa',
                    'Obiekt handlowy' => 'Obiekt handlowy',
                    'Inne' => 'Inne',
                ),
            )
        );

        $builder->add('surface', NumberType::class, array(
                'label' => 'Powierzchnia:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('width', NumberType::class, array(
                'label' => 'Szerokość:',
                )
        );

        $builder->add('lenght', NumberType::class, array(
                'label' => 'Długość:',
                )
        );

        $builder->add('height', NumberType::class, array(
                'label' => 'Wysokość użytkowa:',
                )
        );

        $builder->add('surface_soc', NumberType::class, array(
                'label' => 'Pow. socjalno-biurowa:',
                )
        );

        $builder->add('gate_count', null, array(
                'label' => 'Ilość i wielkość bram:',
                )
        );

        $builder->add('offer_info', null, array(
                'label' => 'Informacje dodatkowe:',
                )
        );

//        $builder->add('google_recaptcha', GoogleRecaptchaType::class, array(
//                'label' => 'Ochrona antyspamowa',
//                'error_bubbling' => false,
//                'mapped' => false,
//                'constraints' => array(
//                    new GoogleRecaptcha()
//                )
//            )
//        );
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij zapytanie'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact_offer';
    }
}
