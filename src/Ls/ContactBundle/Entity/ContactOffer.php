<?php

namespace Ls\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactOffer
 * @ORM\Table(name="contact_offer")
 * @ORM\Entity
 */
class ContactOffer {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $offer_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $firma;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $offer_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $offer_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $contact_info;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */

    private $localization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $purpose;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @var integer
     */
    private $surface;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @var integer
     */
    private $width;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @var integer
     */
    private $lenght;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @var integer
     */
    private $height;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     * @var integer
     */
    private $surface_soc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $gate_count;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $offer_info;


    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set offer_name
     *
     * @param string $offer_name
     * @return ContactOffer
     */
    public function setOfferName($offer_name) {
        $this->offer_name = $offer_name;

        return $this;
    }

    /**
     * Get offer_name
     *
     * @return string
     */
    public function getOfferName() {
        return $this->offer_name;
    }

    /**
     * Set firma
     *
     * @param string $firma
     * @return ContactOffer
     */
    public function setFirma($firma) {
        $this->firma = $firma;

        return $this;
    }

    /**
     * Get firma
     *
     * @return string
     */
    public function getFirma() {
        return $this->firma;
    }

    /**
     * Set phone
     *
     * @param string $offer_phone
     * @return ContactOffer
     */
    public function setOfferPhone($offer_phone) {
        $this->offer_phone = $offer_phone;

        return $this;
    }

    /**
     * Get offer_phone
     *
     * @return string
     */
    public function getOfferPhone() {
        return $this->offer_phone;
    }

    /**
     * Set adress
     *
     * @param string $adress
     * @return ContactOffer
     */
    public function setAdress($adress) {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress() {
        return $this->adress;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return ContactOffer
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set offer_email
     *
     * @param string $offer_email
     * @return ContactOffer
     */
    public function setOfferEmail($offer_email) {
        $this->offer_email = $offer_email;

        return $this;
    }

    /**
     * Get offer_email
     *
     * @return string
     */
    public function getOfferEmail() {
        return $this->offer_email;
    }


    /**
     * Set contact_info
     *
     * @param string $contact_info
     * @return ContactOffer
     */
    public function setContactInfo($contact_info) {
        $this->contact_info = $contact_info;

        return $this;
    }

    /**
     * Get contact_info
     *
     * @return string
     */
    public function getContactInfo() {
        return $this->contact_info;
    }

    /**
     * Set localization
     *
     * @param string $localization
     * @return ContactOffer
     */
    public function setLocalization($localization) {
        $this->localization = $localization;

        return $this;
    }

    /**
     * Get localization
     *
     * @return string
     */
    public function getLocalization() {
        return $this->localization;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     * @return ContactOffer
     */
    public function setPurpose($purpose) {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose() {
        return $this->purpose;
    }


    /**
     * Set surface
     *
     * @param string $surface
     * @return ContactOffer
     */
    public function setSurface($surface) {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return string
     */
    public function getSurface() {
        return $this->surface;
    }

    /**
     * Set width
     *
     * @param string $surface
     * @return ContactOffer
     */
    public function setWidth($width) {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth() {
        return $this->width;
    }


    /**
     * Set lenght
     *
     * @param string $lenght
     * @return ContactOffer
     */
    public function setLenght($lenght) {
        $this->lenght = $lenght;

        return $this;
    }

    /**
     * Get lenght
     *
     * @return string
     */
    public function getLenght() {
        return $this->lenght;
    }    

    /**
     * Set height
     *
     * @param string $height
     * @return ContactOffer
     */
    public function setHeight($height) {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight() {
        return $this->height;
    }    


    /**
     * Set surface_soc
     *
     * @param string $surface_soc
     * @return ContactOffer
     */
    public function setSurfaceSoc($surface_soc) {
        $this->surface_soc = $surface_soc;

        return $this;
    }

    /**
     * Get surface_soc
     *
     * @return string
     */
    public function getSurfaceSoc() {
        return $this->surface_soc;
    }    

    /**
     * Set gate_count
     *
     * @param string $gate_count
     * @return ContactOffer
     */
    public function setGateCount($gate_count) {
        $this->gate_count = $gate_count;

        return $this;
    }

    /**
     * Get gate_count
     *
     * @return string
     */
    public function getGateCount() {
        return $this->gate_count;
    }    

    /**
     * Set offer_info
     *
     * @param string $offer_info
     * @return ContactOffer
     */
    public function setOfferInfo($offer_info) {
        $this->offer_info = $offer_info;

        return $this;
    }

    /**
     * Get gate_count
     *
     * @return string
     */
    public function getOfferInfo() {
        return $this->offer_info;
    }    


    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return ContactOffer
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    public function __toString() {
        if (is_null($this->getOfferName())) {
            return 'NULL';
        }
        return $this->getOfferName();
    }
}