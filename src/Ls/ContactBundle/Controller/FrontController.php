<?php

namespace Ls\ContactBundle\Controller;

use Ls\ContactBundle\Entity\Contact;
use Ls\ContactBundle\Entity\ContactOffer;
use Ls\ContactBundle\Form\ContactType;
use Ls\ContactBundle\Form\ContactOfferType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class FrontController extends Controller {
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Contact();
        $url = $this->generateUrl('ls_contact');
        $form = $this->createForm(ContactType::class, $entity, array(
            'method' => 'POST',
            'action' => $this->generateUrl('ls_contact'),
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $subject = 'Wiadomość z formularza kontaktowego';

            $message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
            $message_txt .= nl2br($entity->getContent()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię i nazwisko: ' . $entity->getName() . '<br />';
            $message_txt .= 'Adres e-mail: ' . $entity->getEmail() . '<br />';
            $message_txt .= 'Numer telefonu: ' . $entity->getPhone() . '<br />';
            $message_txt .= 'Godziny kontaktu: ' . $entity->getContactTime();

            $recipient_string = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_contact')->getValue();
            $recipient_array = explode(',', $recipient_string);
            $recipients = array_map('trim', $recipient_array);

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($recipients);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');

            $mailer->send($message);

            $this->get('session')->getFlashBag()->add('success', 'Twoja wiadomość została wysłana.');

            return new Response('OK');
        }

        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        //Forzmularz "Zapytanie ofertowe"
        $offer_entity = new ContactOffer();

        $offer_form = $this->createForm(ContactOfferType::class, $offer_entity, array(
            'action' => $this->container->get('router')->generate('ls_contact'),
            'method' => 'POST',
        ));

        $offer_form->handleRequest($request);
        if ($offer_form->isValid()) {
            $em->persist($offer_entity);
            $em->flush();

            $subject = 'Wiadomość z formularza "Zapytanie ofertowe"';

            $message_txt = '<h3>Dane kontaktowe: </h3>';
            $message_txt .= 'Imię i nazwisko: ' . $offer_entity->getOfferName() . '<br />';
            $message_txt .= 'Firma: ' . $offer_entity->getFirma() . '<br />';
            $message_txt .= 'Telefon: ' . $offer_entity->getOfferPhone() . '<br />';
            $message_txt .= 'Adres: ' . $offer_entity->getAdress() . '<br />';
            $message_txt .= 'Fax: ' . $offer_entity->getFax() . '<br />';
            $message_txt .= 'E-mail: ' . $offer_entity->getOfferEmail() . '<br />';
            $message_txt .= 'Informacje dodatkowe: ' . $offer_entity->getContactInfo() . '<br />';

            $message_txt .= '<h3>Dane dotyczące inwestycji:</h3>';
            $message_txt .= 'Planowana lokalizacja: ' . $offer_entity->getLocalization() . '<br />';
            $message_txt .= 'Przeznaczenie obiektu: ' . $offer_entity->getPurpose() . '<br />';
            $message_txt .= 'Powierzchnia: ' . $offer_entity->getSurface() . '<br />';
            $message_txt .= 'Szerokość: ' . $offer_entity->getWidth() . '<br />';
            $message_txt .= 'Długość: ' . $offer_entity->getLenght() . '<br />';
            $message_txt .= 'Wysokość użytkowa: ' . $offer_entity->getHeight() . '<br />';
            $message_txt .= 'Pow. socjalno-biurowa: ' . $offer_entity->getSurfaceSoc() . '<br />';
            $message_txt .= 'Ilość i wielkość bram: ' . $offer_entity->getGateCount() . '<br />';
            $message_txt .= 'Informacje dodatkowe: ' . $offer_entity->getOfferInfo() . '<br />';

            $recipient_string = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_contact')->getValue();
            $recipient_array = explode(',', $recipient_string);
            $recipients = array_map('trim', $recipient_array);

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($recipients);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);

            $this->get('session')->getFlashBag()->add('success', 'Twoje zapytanie zostało wysłane.');

            return new Response('OK');
        }
        if ($offer_form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        return $this->render('LsContactBundle:Front:index.html.twig', array(
            'form' => $form->createView(),
            'offer_form' => $offer_form->createView(),
            'url' => $url,
        ));
    }
}
