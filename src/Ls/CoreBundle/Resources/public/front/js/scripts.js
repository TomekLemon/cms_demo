$(document).on('click', '#show-menu', function () {
    if (!$('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeIn(250);
    }
});

$(document).on('click', '.hide-menu', function () {
    if ($('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeOut(250);
    }
});

$('.bxslider').bxSlider({
            mode: 'fade',
            pagerCustom: '#bx-pager',
            auto: true,
            speed: 1000,
            slideSelector: 'li',
            controls: false,
        });

var slider;

slider=$('.partners-img').bxSlider({
    auto: true,
    pager: false,
    speed: 2000,
    minSlides: 1,
    maxSlides: 4,
    slideWidth: 250,
    slideMargin: 0,
    controls: false,
    onSliderResize: reloadBX,
    onSliderLoad: checkBX,
});

/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
  localStorage.cache = idx;
  // Reloads slider, 
  ///goes back to the slide it was on before resize,
  ///removes the stored index.
  function inner(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
  }
}

/*
Load Callback
*/ // If the slider height is collapsed, 
 /////invoke a repaint and stay on current index.
function checkBX(idx) {
  var vp = $('.bx-viewport').height();
  while (vp <= 0) {
    slider.redrawSlider();
  }
  slider.goToSlide(idx);
}

jQuery(document).ready(function() {
  jQuery(".animate-link").click(function () {
    event.preventDefault();
    elementClick = jQuery(this).attr("href")
    
    destination = jQuery(elementClick).offset().top - 100;
    jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
    return false;
  });
});

/*
function setCookie(c_name,value,exdays) {
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape(value) + ((exdays==null) ? "" : ";     expires="+exdate.toUTCString());
  document.cookie=c_name + "=" + c_value;
}*/

function closePopup(path) {
  $.ajax({
      type: "GET",
      url: path,
      dataType: "json", 
      success: function (response) {
      },
      error: function (response) {
      }
  });
}