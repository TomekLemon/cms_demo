$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Tekst 1', element: 'div', attributes: {'class': 'text1'}},
            {name: 'Tekst 2', element: 'div', attributes: {'class': 'text2'}},
            {name: 'Kolor', element: 'span', attributes: {'class': 'orange'}},
            {name: 'Wypunktowanie', element: 'div', attributes: {'class': 'list-style'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=flash'
    };
    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);
    $('.wysiwyg').ckeditor(fckconfig);
});

