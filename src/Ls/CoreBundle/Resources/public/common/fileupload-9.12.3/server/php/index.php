<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');


$error_messages = array();

$options = array(
    'image_versions' => array(),
);

if (isset($_REQUEST['custom_dir'])) {
    $options['upload_dir'] = $_REQUEST['custom_dir'];
}

if (isset($_REQUEST['min_width'])) {
    $options['min_width'] = $_REQUEST['min_width'];
    $error_messages['min_width'] = 'Szerokość zdjęcia mniejsza niż ' . $options['min_width'] . 'px';
}

if (isset($_REQUEST['min_height'])) {
    $options['min_height'] = $_REQUEST['min_height'];
    $error_messages['min_height'] = 'Wysokość zdjęcia mniejsza niż ' . $options['min_height'] . 'px';
}

$upload_handler = new UploadHandler($options, true, $error_messages);
