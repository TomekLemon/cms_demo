<?php

namespace Ls\SliderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Slider
 * @ORM\Table(name="slider")
 * @ORM\Entity
 */
class Slider {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190, unique=true)
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $photo_width;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $photo_height;

    /**
     * @ORM\OneToMany(
     *   targetEntity="SliderPhoto",
     *   mappedBy="slider",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->photos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Slider
     */
    public function setLabel($label) {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * Set photo_full_width
     *
     * @param integer $photo_width
     * @return Slider
     */
    public function setPhotoWidth($photo_width) {
        $this->photo_width = $photo_width;

        return $this;
    }

    /**
     * Get photo_width
     *
     * @return integer
     */
    public function getPhotoWidth() {
        return $this->photo_width;
    }

    /**
     * Set photo_height
     *
     * @param integer $photo_height
     * @return Slider
     */
    public function setPhotoHeight($photo_height) {
        $this->photo_height = $photo_height;

        return $this;
    }

    /**
     * Get photo_height
     *
     * @return integer
     */
    public function getPhotoHeight() {
        return $this->photo_height;
    }

    /**
     * Add photos
     *
     * @param SliderPhoto $photo
     * @return Slider
     */
    public function addPhoto(SliderPhoto $photo) {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param SliderPhoto $photo
     */
    public function removePhoto(SliderPhoto $photo) {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos() {
        return $this->photos;
    }


    public function __toString() {
        if (is_null($this->getLabel())) {
            return 'NULL';
        }
        return $this->getLabel();
    }

}