<?php

namespace Ls\OfferBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * OfferProjects
 * @ORM\Table(name="offer_examples")
 * @ORM\Entity
 */
class OfferProjects
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $offer_id;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $realization_id;

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set offer_id
     *
     * @param integer $offer_id
     * @return OfferProjects
     */
    public function setOfferId($offer_id)
    {
        $this->offer_id = $offer_id;

        return $this;
    }

    /**
     * Get offer_id
     *
     * @return integer
     */
    public function getOfferId()
    {
        return $this->offer_id;
    }

    /**
     * Set realization_id
     *
     * @param integer $realization_id
     * @return OfferProjects
     */
    public function setRealizationId($realization_id)
    {
        $this->realization_id = $realization_id;

        return $this;
    }

    /**
     * Get realizations
     *
     * @return string
     */
    public function getRealizationId()
    {
        return $this->realization_id;
    }

    /**
     * Get realizations
     *
     * @return string
     */
    public function getRealizations()
    {
        return $this->realizations;
    }
}