<?php

namespace Ls\OfferBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Offer controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Offer entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();


        return $this->render('LsOfferBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOfferBundle:Offer', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        return $this->render('LsOfferBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_entities')->getValue();
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        foreach ($entities as $entity) {
            if ($entity->getPhotoWebPath()) {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 170, '...'));
            } else {
                $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 255, '...'));
            }
        }

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsFormBundle:Offer:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

        public function blockAction($offer_id) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $examples_list = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOfferBundle:OfferProjects', 'p')
            ->where('p.offer_id = :offer_id')
            ->setParameter('offer_id', $offer_id)
            ->getQuery()
            ->getResult();

        $realizations_id = [];

        foreach ($examples_list as $value) {
            array_push($realizations_id, $value->getRealizationId());
        }


        $entities = $qb->select('a')
            ->from('LsRealizationsBundle:Realizations', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.id IN (:realizations_id)')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setParameter('realizations_id', $realizations_id)
            ->getQuery()
            ->getResult();
        return $this->render('LsOfferBundle:Front:block.html.twig', array(
            'entities' => $entities,
        ));
    }
}
