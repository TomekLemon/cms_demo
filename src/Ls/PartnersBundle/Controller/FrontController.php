<?php

namespace Ls\PartnersBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Partners controller.
 *
 */
class FrontController extends Controller {

    public function blockAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsPartnersBundle:Partners', 'a')
            ->orderBy('a.id', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsPartnersBundle:Front:block.html.twig', array(
            'entities' => $entities,
        ));
    }
}
