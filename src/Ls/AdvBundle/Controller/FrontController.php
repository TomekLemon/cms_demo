<?php

namespace Ls\AdvBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
/**
 * Adv controller.
 *
 */
class FrontController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsAdvBundle:Adv', 'a')
            ->setMaxResults(1)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getSingleResult();

        $session = new Session();

        if (!$session->has('popup')) {
            $session->set('popup',  $entity->getCookieId());
        }

        return $this->render('LsAdvBundle:Front:index.html.twig', array(
            'entity' => $entity,
        ));
    }

    public function closeAction(Request $request, $value) {

    	if($request->isXmlHttpRequest()) {
    		$session = new Session();
           	$session->set('popup',  $value);
        }
        else {
        	throw $this->createNotFoundException('Ajax request only');
        }

        return new Response('OK');
    }
}
