<?php

namespace Ls\AdvBundle\Controller;

use Ls\AdvBundle\Entity\Adv;
use Ls\AdvBundle\Form\AdvType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller { 

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsAdvBundle:Adv')->find(1);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find adv entity.');
        }
        //$size = $entity->getThumbSize('list');

        $form = $this->createForm(AdvType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_adv'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $entity->deletePhoto();

                $sFileName = uniqid('adv-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_adv'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Moduł reklamowy', $this->get('router')->generate('ls_admin_adv'));

        return $this->render('LsAdvBundle:Admin:index.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));
    }
}
