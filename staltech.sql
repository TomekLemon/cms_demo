-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Paź 2016, 16:21
-- Wersja serwera: 5.7.11
-- Wersja PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `staltech`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `banners`
--

INSERT INTO `banners` (`id`, `title`, `link`, `photo`) VALUES
(1, 'Sprawdź nas na OLX', '#', 'banner-image-57f4b8d68dfaa.png'),
(2, 'Zobacz nas na Allegro', '#', 'banner-image-57f4b9271bc5f.png'),
(3, 'Nasze certyfikaty', '#', 'banner-image-57f4b949ac486.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `contact_time`, `content`, `created_at`) VALUES
(1, 'fghgfh', 'fghfh@f.bd', 'gfhfghfg', '12:00 - 16:00', 'fghfghfghf', '2016-09-23 10:12:55');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact_offer`
--

CREATE TABLE `contact_offer` (
  `id` int(11) NOT NULL,
  `offer_name` varchar(255) DEFAULT NULL,
  `firma` varchar(255) DEFAULT NULL,
  `offer_phone` varchar(255) DEFAULT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `offer_email` varchar(255) DEFAULT NULL,
  `contact_info` varchar(255) DEFAULT NULL,
  `localization` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `surface` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `lenght` int(10) DEFAULT NULL,
  `height` int(10) DEFAULT NULL,
  `surface_soc` int(10) DEFAULT NULL,
  `gate_count` varchar(255) DEFAULT NULL,
  `offer_info` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `contact_offer`
--

INSERT INTO `contact_offer` (`id`, `offer_name`, `firma`, `offer_phone`, `adress`, `fax`, `offer_email`, `contact_info`, `localization`, `purpose`, `surface`, `width`, `lenght`, `height`, `surface_soc`, `gate_count`, `offer_info`, `created_at`) VALUES
(2, '3453', '535', '+5', '35345', NULL, 'vaskiv7@gmail.com', NULL, '4354355', 'Hala produkcyjna', 3535, 5353, 535, 3453, 5353, '34534535', NULL, '2016-09-23 10:37:39');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `old_slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 'Aktualność 1', 'aktualnosc-1', 'aktualnosc-1', 1, 1, 0, 'Aktualność 1', NULL, NULL, '2016-09-26 08:44:19', '2016-09-26 08:44:20');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(1, 1, 'gallery-image-57e8e06c58c78.jpg', 3),
(2, 1, 'gallery-image-57e8e06c82378.jpg', 1),
(3, 1, 'gallery-image-57e8e06c88e65.png', 2),
(4, 1, 'gallery-image-57e8e74118ad6.jpg', 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'route', 'Strona główna', 'ls_core_homepage', NULL, '/', NULL, 0, 1),
(2, NULL, 'menu_top', 'route', 'O nas', 'ls_page_show', '{"slug":"o-nas"}', '/', NULL, 0, 2),
(3, NULL, 'menu_top', 'route', 'Aktualności', 'ls_news', NULL, '/', NULL, 0, 3),
(4, NULL, 'menu_top', 'url', 'Oferta', NULL, NULL, '/', NULL, 0, 4),
(5, NULL, 'menu_top', 'route', 'Realizacje', 'ls_realizations', NULL, '/', NULL, 0, 5),
(6, NULL, 'menu_top', 'route', 'Kontakt', 'ls_contact', NULL, '/', NULL, 0, 6),
(7, NULL, 'menu_bottom', 'route', 'Strona główna', 'ls_core_homepage', NULL, NULL, NULL, 0, 1),
(8, NULL, 'menu_bottom', 'route', 'O nas', 'ls_page_show', '{"slug":"o-nas"}', '/', NULL, 0, 2),
(9, NULL, 'menu_bottom', 'url', 'Aktualności', NULL, NULL, '/', NULL, 0, 3),
(10, NULL, 'menu_bottom', 'url', 'Oferta', NULL, NULL, '/', NULL, 0, 4),
(11, NULL, 'menu_bottom', 'url', 'Realizacje', NULL, NULL, '/', NULL, 0, 5),
(12, NULL, 'menu_bottom', 'url', 'Allegro', NULL, NULL, '/', NULL, 0, 7),
(13, NULL, 'menu_bottom', 'url', 'Certyfikaty', NULL, NULL, '/', NULL, 0, 8),
(14, NULL, 'menu_bottom', 'url', 'Kontakt', NULL, NULL, '/', NULL, 0, 6);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20160816084137'),
('20160823112303');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `show_main`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(1, 1, NULL, 'Targi rolnicze Zielone  Agro Show', 'targi-rolnicze-zielone-agro-show', 'targi-rolnicze-zielone-agro-show', 0, 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '<div class="text2">\r\n	W dniach 4-5 czerwca 2016r., na terenie Moto Parku w Ułężu, woj. lubelskie STALTECH JACEK SIECIECHOWICZ miał okazje uczestniczyć jako wystawca&nbsp;<span style="font-family: Ubuntu-Medium;">w Targach Rolniczych Zielone AGRO SHOW</span><span class="normal">.&nbsp;</span>Podczas targ&oacute;w Nasza firma miała możliwość zaprezentowania nowego produktu wchodzącego do Naszej oferty - <span class="normal">hal łukowych.</span></div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Więcej informacji znajdziesz - <a href="/">tutaj</a></div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Jest to zupełnie nowy produkt produkowany przez firmę ARCELOR MITTAL &ndash; kt&oacute;rego Dystrybutorem właśnie się staliśmy. W trakcie targ&oacute;w prezentowaliśmy r&oacute;wnież Nasz sztandarowy projekt tj;&nbsp;<span class="normal">hale typowe z dźwigarem kratowym, bez podpory w środku,&nbsp;</span>kt&oacute;ra idealnie sprawdza się jako r&oacute;żnego typu hale - magazynowe, inwentarskie, rolnicze, chłodnie i inne.</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="normal">\r\n	<span class="orange">Zapraszamy do zapoznania się z rozszerzoną ofertą i składania zapytań ofertowych.</span></div>', 'news-image-57e52bd87fd4b.jpeg', 0, 'Targi rolnicze Zielone  Agro Show', 'Targi, rolnicze, Zielone, Agro, Show, Różnorodność, wykonywanych, usług, pozwoliła, doskonalenie', 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '2016-09-23 13:19:19', '2016-09-26 09:20:09', '2016-06-10 13:16:00'),
(2, NULL, NULL, 'Obecny stan realizacji  firmy StalTech', 'obecny-stan-realizacji-firmy-staltech', 'obecny-stan-realizacji-firmy-staltech', 0, 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '<p>\r\n	R&oacute;żnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dob&oacute;r odpowiedniej techniki pracy.&nbsp;</p>', 'news-image-57e52c7d6ed86.png', 0, 'Obecny stan realizacji  firmy StalTech', 'Obecny, stan, realizacji, firmy, StalTech, Różnorodność, wykonywanych, usług, pozwoliła, doskonalenie', 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '2016-09-23 13:22:05', '2016-09-23 13:22:05', '2016-06-03 13:03:00'),
(3, NULL, NULL, 'Nowe realizacje', 'nowe-realizacje', 'nowe-realizacje', 0, 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '<p>\r\n	R&oacute;żnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dob&oacute;r odpowiedniej techniki pracy.&nbsp;</p>', 'news-image-57e52caa17e4b.jpeg', 0, 'Nowe realizacje', 'Nowe, realizacje, Różnorodność, wykonywanych, usług, pozwoliła, doskonalenie, się, kadry, dobór', 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '2016-09-23 13:22:49', '2016-09-23 13:22:50', '2016-05-30 13:03:00'),
(4, NULL, NULL, 'StalTech na targach EXPO', 'staltech-na-targach-expo', 'staltech-na-targach-expo', 0, 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '<p>\r\n	R&oacute;żnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dob&oacute;r odpowiedniej techniki pracy.&nbsp;</p>', 'news-image-57e52e78e31bb.jpeg', 0, 'StalTech na targach EXPO', 'StalTech, targach, EXPO, Różnorodność, wykonywanych, usług, pozwoliła, doskonalenie, się, kadry', 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '2016-09-23 13:30:32', '2016-09-23 13:30:32', '2016-04-11 13:03:00'),
(5, NULL, NULL, 'Most o konstrukcji stalowej nad Wisłą', 'most-o-konstrukcji-stalowej-nad-wisla', 'most-o-konstrukcji-stalowej-nad-wisla', 0, 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '<p>\r\n	R&oacute;żnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dob&oacute;r odpowiedniej techniki pracy.&nbsp;</p>', 'news-image-57e52f3a31020.jpeg', 0, 'Most o konstrukcji stalowej nad Wisłą', 'Most, konstrukcji, stalowej, Wisłą, Różnorodność, wykonywanych, usług, pozwoliła, doskonalenie, się', 'Różnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dobór odpowiedniej techniki pracy.', '2016-09-23 13:33:45', '2016-09-23 13:33:46', '2016-03-27 13:03:00'),
(6, 1, NULL, 'Targi rolnicze Zielone Agro Show', 'targi-rolnicze-zielone-agro-show-1', 'targi-rolnicze-zielone-agro-show-1', 0, 'W dniach 4-5 czerwca 2016r., na terenie Moto Parku w Ułężu, woj. lubelskie STALTECH JACEK SIECIECHOWICZ miał okazje uczestniczyć jako wystawca w Targach Rolniczych Zielone AGRO SHOW. Podczas targów Nasza firma miała możliwość zaprezentowania nowego...', '<div class="text2" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	W dniach 4-5 czerwca 2016r., na terenie Moto Parku w Ułężu, woj. lubelskie STALTECH JACEK SIECIECHOWICZ miał okazje uczestniczyć jako wystawca&nbsp;<span style="box-sizing: border-box; font-family: Ubuntu-Medium;">w Targach Rolniczych Zielone AGRO SHOW</span><span class="normal" style="box-sizing: border-box;">.&nbsp;</span>Podczas targ&oacute;w Nasza firma miała możliwość zaprezentowania nowego produktu wchodzącego do Naszej oferty -&nbsp;<span class="normal" style="box-sizing: border-box;">hal łukowych.</span></div>\r\n<div class="text2" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	&nbsp;</div>\r\n<div class="text2" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	Więcej informacji znajdziesz -&nbsp;<a href="http://staltech.loc/" style="box-sizing: border-box; margin: 0px; padding: 0px; background-color: transparent;">tutaj</a></div>\r\n<div class="text2" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	&nbsp;</div>\r\n<div class="text2" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	Jest to zupełnie nowy produkt produkowany przez firmę ARCELOR MITTAL &ndash; kt&oacute;rego Dystrybutorem właśnie się staliśmy. W trakcie targ&oacute;w prezentowaliśmy r&oacute;wnież Nasz sztandarowy projekt tj;&nbsp;<span class="normal" style="box-sizing: border-box;">hale typowe z dźwigarem kratowym, bez podpory w środku,&nbsp;</span>kt&oacute;ra idealnie sprawdza się jako r&oacute;żnego typu hale - magazynowe, inwentarskie, rolnicze, chłodnie i inne.</div>\r\n<div class="normal" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	&nbsp;</div>\r\n<div class="normal" style="box-sizing: border-box; background-color: rgb(255, 255, 255);">\r\n	<span class="orange" style="box-sizing: border-box;">Zapraszamy do zapoznania się z rozszerzoną ofertą i składania zapytań ofertowych.</span></div>', 'news-image-57e9102c19d65.jpeg', 0, 'Targi rolnicze Zielone Agro Show', 'Targi, rolnicze, Zielone, Agro, Show, dniach, czerwca, 2016r, terenie, Moto', 'W dniach 4-5 czerwca 2016r., na terenie Moto Parku w Ułężu, woj. lubelskie STALTECH JACEK SIECIECHOWICZ miał okazje uczestniczyć jako wystawca w Targach...', '2016-09-26 12:10:19', '2016-09-26 13:59:17', '2015-09-24 12:09:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'O nas', 'o-nas', 'o-nas', 0, 'Nasza firma powstała na przełomie lat 90-tych, nasze pierwsze działania skierowane były na produkcję elementów maszyn budowlanych. Dalszy rozwój firmy to opanowanie pokrewnych rynków, zajmujących się konstrukcjami stalowymi. Różnorodność...', '<p class="normal">\r\n	Nasza firma powstała na przełomie lat 90-tych, nasze pierwsze działania skierowane były na produkcję element&oacute;w maszyn budowlanych. Dalszy rozw&oacute;j firmy to opanowanie pokrewnych rynk&oacute;w, zajmujących się konstrukcjami stalowymi.&nbsp;</p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	R&oacute;żnorodność wykonywanych usług pozwoliła na doskonalenie się kadry i dob&oacute;r odpowiedniej techniki pracy. Po wielu latach działalności firma StalTech wsparta najnowszymi technologiami oraz swoją wiedzą oferuje Państwu najwyższej jakości usługi w</p>\r\n<p class="text1">\r\n	dziedzinie produkcji konstrukcji stalowych.</p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	Działalność firmy StalTech można podzielić na dwie podstawowe gałęzie tzn.: projektowa oraz produkcyjno-montażowa.</p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	<img alt="" src="/upload/pliki/Layer%2059.png" style="width: 588px; height: 260px;" /></p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	W zakres działalności projektowej wchodzi kompleksowa obsługa inwestycji od strony formalno-projektowej.<a href="/"> Przykład linku tekstowego</a> Posiadamy własne jednostki projektowe z ponad 15-sto letnim doświadczeniem. Posiadamy r&oacute;wnież dużą bazę projekt&oacute;w powtarzalnych, typowych, jak r&oacute;wnież robimy projekty indywidualne.</p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	Drugą gałęzią działalności naszej firmy jest produkcja konstrukcji stalowych oraz całość prac budowlano-montażowych przy budowie budynku o konstrukcji stalowej. Posiadamy własne zaplecze technologiczne oraz kompletny park maszynowy. <span class="orange">Całość procesu produkcyjnego odbywa się na terenie naszego zakładu, począwszy od prefabrykacji, przez odtłuszczenie (piaskowanie), po finalne nakładanie powłoki antykorozyjnej. </span>Do zabezpieczenia konstrukcji stalowej przed zewnętrznymi warunkami używamy farb epoksydowych, chemoutwardzalnych z kompletem atest&oacute;w higienicznych i sanitarnych.</p>\r\n<p class="text1">\r\n	&nbsp;</p>\r\n<p class="text1">\r\n	Firma StalTech oferuje:</p>\r\n<p class="text1">\r\n	Prace Projektowe &ndash; kompleksowa obsługa formalno-prawna przy uzyskaniu pozwolenia na budowę</p>\r\n<p class="text1">\r\n	Produkcję konstrukcji stalowych</p>\r\n<p class="text1">\r\n	Montaż konstrukcji stalowych</p>\r\n<p class="text1">\r\n	Wykonywanie prac ziemnych, fundamenty, podwalina, posadzki, podjazdy, murowane ściany działowe</p>\r\n<p class="text1">\r\n	Wykonanie kompleksowej obudowy płytą warstwową wraz z orynnowaniem, bramami, świetlikami</p>\r\n<p class="text1">\r\n	Wykonanie kompleksowej obudowy blachą trapezową</p>', 0, 'O nas', NULL, 'O nas', '2016-09-21 13:19:00', '2016-09-22 10:07:41');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `realizations`
--

CREATE TABLE `realizations` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `arrangement` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT '-',
  `industry` varchar(255) DEFAULT '-',
  `province` varchar(255) DEFAULT '-',
  `city` varchar(255) DEFAULT '-',
  `size_param` varchar(255) DEFAULT '-',
  `latitude` varchar(25) DEFAULT NULL,
  `longitude` varchar(25) DEFAULT NULL,
  `marker_type` int(1) DEFAULT '1',
  `content` longtext NOT NULL,
  `slug` varchar(255) NOT NULL,
  `old_slug` varchar(255) DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `realizations`
--

INSERT INTO `realizations` (`id`, `gallery_id`, `arrangement`, `title`, `type`, `industry`, `province`, `city`, `size_param`, `latitude`, `longitude`, `marker_type`, `content`, `slug`, `old_slug`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(1, NULL, NULL, 'Fabryka drzwi Derpal', 'Hala produkcyjna', '-', 'lubelskie', 'Janów Lubelski', '18 / 27 / 5', '50.70675237289231', '22.41063416004181', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text1">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text1">\r\n	&nbsp;</div>\r\n<div class="text1">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'fabryka-drzwi-derpal', 'fabryka-drzwi-derpal', 0, 'Fabryka drzwi Derpal', 'Fabryka, drzwi, Derpal, Proin, cursus, massa, varius, Nunc, quam, quis', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-05 13:50:06', '2016-10-06 12:35:04', '2016-10-05 13:44:00'),
(2, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt handlowo-magazynowy', 'Handlowa', 'lubelskie', 'Poluszowice - Kolonia', NULL, '51.260733228426076', '22.440004348754883', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'centrum-ogrodnicze-premium-garden', 'centrum-ogrodnicze-premium-garden', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 10:03:27', '2016-10-06 10:25:59', '2016-10-06 10:01:00'),
(3, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Obiekt handlowy', 'Handlowa', 'podkarpackie', 'Stalowa Wola', NULL, '50.57619212330895', '22.061222791671753', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy', 'dom-gaz-s-j-madej-z-a-i-wspolnicy', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 10:28:55', '2016-10-06 10:32:17', '2016-10-06 10:26:00'),
(6, NULL, NULL, 'Szkółka drzewek owocowych - Anasiewicz', 'Obiekt magazynowy', 'Handlowa', 'podkarpackie', 'Rzeszów', NULL, '50.03917108313303', '22.002182006835938', 3, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'szkolka-drzewek-owocowych-anasiewicz', 'szkolka-drzewek-owocowych-anasiewicz', 0, 'Szkółka drzewek owocowych - Anasiewicz', 'Szkółka, drzewek, owocowych, Anasiewicz, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:09:36', '2016-10-06 11:09:36', '2016-10-06 11:08:00'),
(7, NULL, NULL, 'Szkółka drzewek owocowych - Anasiewicz', 'Obiekt magazynowy', 'Handlowa', 'małopolskie', 'Kraków', NULL, '50.04302974380059', '19.924392700195312', 2, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'szkolka-drzewek-owocowych-anasiewicz-1', 'szkolka-drzewek-owocowych-anasiewicz-1', 0, 'Szkółka drzewek owocowych - Anasiewicz', 'Szkółka, drzewek, owocowych, Anasiewicz, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:11:00', '2016-10-06 11:13:47', '2016-10-06 11:10:00'),
(8, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Hala produkcyjna', 'Handlowa', 'małopolskie', 'Bochnia', NULL, '49.96314743329141', '20.42083740234375', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-1', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-1', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:13:31', '2016-10-06 11:13:32', '2016-10-06 11:11:00'),
(9, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt magazynowy', 'Handlowa', 'podkarpackie', 'Ropczyce', NULL, '50.04986432424856', '21.611480712890625', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'centrum-ogrodnicze-premium-garden-1', 'centrum-ogrodnicze-premium-garden-1', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:14:39', '2016-10-06 11:14:40', '2016-10-06 11:11:00'),
(10, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Obiekt handlowy', 'Handlowa', 'podkarpackie', 'Kupno', NULL, '50.201956791060844', '21.838417053222656', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-2', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-2', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:15:19', '2016-10-06 11:15:20', '2016-10-06 11:11:00'),
(11, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Hala produkcyjna', 'Handlowa', 'małopolskie', 'Rudnik', NULL, '49.91785133183984', '20.086612701416016', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-3', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-3', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:15:58', '2016-10-06 11:15:59', '2016-10-06 11:11:00'),
(12, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt magazynowy', 'Handlowa', 'podkarpackie', 'Mielec', NULL, '50.28407449795535', '21.4508056640625', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'centrum-ogrodnicze-premium-garden-2', 'centrum-ogrodnicze-premium-garden-2', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:16:33', '2016-10-06 11:16:34', '2016-10-06 11:11:00'),
(13, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Hala produkcyjna', 'Handlowa', 'lubelskie', 'Rudnik Drugi', NULL, '50.94328650904304', '22.458114624023438', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-4', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-4', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-06 11:17:12', '2016-10-06 11:17:13', '2016-10-06 11:11:00'),
(14, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', '-', '-', '-', '-', NULL, '50.28933925329177', '20.8355712890625', 1, '<p>\r\n	Dom Gaz S.J. Madej Z.A. i Wsp&oacute;lnicy</p>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-5', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-5', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy', 'Dom Gaz S.J. Madej Z.A. i Wspólnicy Dom Gaz S.J. Madej Z.A. i Wspólnicy', '2016-10-07 12:32:33', '2016-10-07 12:35:11', '2016-10-07 12:31:00'),
(15, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt magazynowy', 'Handlowa', 'lubelskie', 'Poluszowice - Kolonia', NULL, '50.16634316943973', '21.5771484375', 1, '<p>\r\n	htyjtyjt</p>', 'centrum-ogrodnicze-premium-garden-3', 'centrum-ogrodnicze-premium-garden-3', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, htyjtyjt', 'htyjtyjt', '2016-10-07 12:35:53', '2016-10-07 12:35:54', '2016-10-07 12:31:00'),
(16, NULL, NULL, 'Fabryka drzwi Derpal', 'Obiekt magazynowy', 'Handlowa', 'podkarpackie', 'Stalowa Wola', '18 / 27 / 5', '50.33143633083883', '22.159423828125', 1, '<p>\r\n	yjtjtjt</p>', 'fabryka-drzwi-derpal-1', 'fabryka-drzwi-derpal-1', 0, 'Fabryka drzwi Derpal', 'Fabryka, drzwi, Derpal, yjtjtjt', 'yjtjtjt', '2016-10-07 12:35:57', '2016-10-07 12:35:58', '2016-10-07 12:31:00'),
(17, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt handlowy', 'Handlowa', 'małopolskie', 'Janów Lubelski', '18 / 27 / 5', '49.82735281650853', '21.1322021484375', 1, '<p>\r\n	jtjytjt</p>', 'centrum-ogrodnicze-premium-garden-4', 'centrum-ogrodnicze-premium-garden-4', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, jtjytjt', 'jtjytjt', '2016-10-07 12:36:17', '2016-10-07 12:36:18', '2016-10-07 12:31:00'),
(18, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'gfbfgbfb', '-', 'podkarpackie', '-', NULL, '49.986552130506176', '22.137451171875', 1, '<p>\r\n	ghngn</p>', 'centrum-ogrodnicze-premium-garden-5', 'centrum-ogrodnicze-premium-garden-5', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, ghngn', 'ghngn', '2016-10-07 12:36:56', '2016-10-07 12:36:57', '2016-10-07 12:31:00'),
(19, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', 'Obiekt handlowy', '-', 'lubelskie', '-', NULL, '50.30337575356313', '21.11572265625', 1, '<p>\r\n	ghgngngn</p>', 'centrum-ogrodnicze-premium-garden-6', 'centrum-ogrodnicze-premium-garden-6', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, ghgngngn', 'ghgngngn', '2016-10-07 12:37:08', '2016-10-07 12:37:08', '2016-10-07 12:31:00'),
(20, NULL, NULL, 'Fabryka drzwi Derpal', 'Hala produkcyjna', 'Handlowa', 'lubelskie', 'Miasto 1', NULL, '49.73868163928003', '19.951171875', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'fabryka-drzwi-derpal-2', 'fabryka-drzwi-derpal-2', 0, 'Fabryka drzwi Derpal', 'Fabryka, drzwi, Derpal, Proin, cursus, massa, varius, Nunc, quam, quis', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-07 12:40:16', '2016-10-07 12:40:17', '2016-10-07 12:37:00'),
(21, NULL, NULL, 'Szkółka drzewek owocowych - Anasiewicz', 'Obiekt handlowo-magazynowy', 'Handlowa', 'lubelskie', 'Miasto 1', NULL, '49.7315809334801', '20.6268310546875', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'szkolka-drzewek-owocowych-anasiewicz-2', 'szkolka-drzewek-owocowych-anasiewicz-2', 0, 'Szkółka drzewek owocowych - Anasiewicz', 'Szkółka, drzewek, owocowych, Anasiewicz, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-07 12:40:50', '2016-10-07 12:40:50', '2016-10-07 12:37:00'),
(22, NULL, NULL, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Obiekt handlowy', 'Handlowa', 'podkarpackie', 'Rzeszów', '18 / 27 / 5', '50.138185230436896', '22.005615234375', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-6', 'dom-gaz-s-j-madej-z-a-i-wspolnicy-6', 0, 'Dom Gaz S.J. Madej Z.A. i Wspólnicy', 'Madej, Wspólnicy, Proin, cursus, massa, varius, Nunc, quam, quis, risus', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-07 12:41:18', '2016-10-07 12:41:19', '2016-10-07 12:37:00'),
(23, NULL, NULL, 'Szkółka drzewek owocowych - Anasiewicz', 'Obiekt handlowy', 'Handlowa', 'małopolskie', 'Janów Lubelski', NULL, '49.87693780072527', '21.5386962890625', 1, '<div class="normal">\r\n	Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus.&nbsp;</div>\r\n<div class="normal">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Cras a erat nisl. Integer ut nunc mauris. In tempus efficitur tellus, et suscipit libero consectetur vitae. Nulla gravida nisi in lorem aliquet consectetur. Nam sit amet metus egestas, iaculis quam vel, hendrerit nibh. Mauris egestas lectus eu orci consectetur ullamcorper. Vivamus varius eget metus in cursus.</div>\r\n<div class="text2">\r\n	&nbsp;</div>\r\n<div class="text2">\r\n	Morbi rhoncus sem sit amet scelerisque malesuada. Nunc pulvinar quam a arcu tempus accumsan. Nunc sit amet nulla id erat efficitur auctor et ac metus. Donec nec ex a mi posuere eleifend. Cras aliquet lectus sem, ut vulputate nibh lacinia sit amet. Sed dolor ligula, dictum et lacinia molestie, condimentum et leo. Nunc feugiat magna justo, id luctus turpis.</div>', 'szkolka-drzewek-owocowych-anasiewicz-3', 'szkolka-drzewek-owocowych-anasiewicz-3', 0, 'Szkółka drzewek owocowych - Anasiewicz', 'Szkółka, drzewek, owocowych, Anasiewicz, Proin, cursus, massa, varius, Nunc, quam', 'Proin cursus a massa vel varius. Nunc vel quam quis risus finibus mattis eu sit amet orci. Vivamus varius eget metus in cursus. Cras a erat nisl. Integer...', '2016-10-07 12:41:39', '2016-10-07 12:41:39', '2016-10-07 12:38:00'),
(62, NULL, NULL, 'Centrum Ogrodnicze Premium Garden', '-', '-', '-', '-', NULL, '49.9193987302727', '22.7471923828125', 2, '<p>\r\n	trthrhr</p>', 'centrum-ogrodnicze-premium-garden-7', 'centrum-ogrodnicze-premium-garden-7', 0, 'Centrum Ogrodnicze Premium Garden', 'Centrum, Ogrodnicze, Premium, Garden, trthrhr', 'trthrhr', '2016-10-10 15:26:58', '2016-10-10 15:43:18', '2016-10-10 15:25:00'),
(63, NULL, NULL, 'ghgh', '-', '-', '-', '-', NULL, NULL, NULL, 1, '<p>\r\n	ghgh</p>', 'ghgh', 'ghgh', 0, 'ghgh', 'ghgh', 'ghgh', '2016-10-10 15:44:18', '2016-10-10 16:20:07', '2016-10-10 15:44:00'),
(64, NULL, NULL, 'fgfgh', '-', '-', '-', '-', NULL, NULL, NULL, 1, '<p>\r\n	fghfh</p>', 'fgfgh', 'fgfgh', 0, 'fgfgh', 'fgfgh, fghfh', 'fghfh', '2016-10-10 15:59:02', '2016-10-10 15:59:03', '2016-10-10 15:58:00'),
(65, NULL, NULL, 'fgfgh', '-', '-', '-', '-', NULL, NULL, NULL, 1, '<p>\r\n	fghfh</p>', 'fgfgh-1', 'fgfgh-1', 0, 'fgfgh', 'fgfgh, fghfh', 'fghfh', '2016-10-10 16:03:07', '2016-10-10 16:03:08', '2016-10-10 15:58:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `realizations_options`
--

CREATE TABLE `realizations_options` (
  `id` int(11) NOT NULL,
  `realization_id` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `realizations_options`
--

INSERT INTO `realizations_options` (`id`, `realization_id`, `label`, `value`) VALUES
(1, 1, NULL, NULL),
(2, 1, 'gfgnfgh2', 'bnfgh3'),
(3, 11, '1', '1'),
(4, 11, '2', '2'),
(5, 11, '3', '3'),
(6, 10, '111', '111'),
(8, NULL, 'dfgd 111', 'fgdfg 111'),
(9, NULL, 'dfgd 111', 'fgdfg 111'),
(10, NULL, 'dfgd 111', 'fgdfg 111'),
(11, NULL, 'fff', 'fff'),
(12, NULL, 'y', 'y'),
(13, NULL, 'j', 'j'),
(24, NULL, '7', '7'),
(23, 62, '6', '6'),
(22, 62, '5', '5'),
(21, 62, '4', '4'),
(19, 62, '2', '2'),
(20, 62, '3', '3'),
(18, 62, '1', '1'),
(25, NULL, '9', '9'),
(26, NULL, '8', '8'),
(27, NULL, '7', '7'),
(28, NULL, '9', '9'),
(29, NULL, '8', '8'),
(30, NULL, '7', '7'),
(31, 62, '9', '9'),
(32, 62, '8', '8'),
(33, NULL, '7', '7'),
(34, NULL, '11', '11'),
(35, NULL, 'yy', 'yy'),
(36, NULL, 'm', 'm'),
(37, NULL, 'j', 'j'),
(38, NULL, 'd', 'd'),
(39, NULL, 'd', 'd'),
(40, NULL, 'f', 'f'),
(41, NULL, 'g', 'g'),
(42, NULL, 'd', 'd'),
(43, NULL, 'h', 'h'),
(44, NULL, 'g', 'g'),
(45, 63, 'h', 'h'),
(46, NULL, 'j', 'j'),
(47, NULL, 'y', 'y'),
(48, NULL, 't', 't'),
(49, NULL, 'h', 'h'),
(50, NULL, 'f', 'f'),
(51, NULL, 'tyut', 'ytutu'),
(52, 64, 'gfhfgh', 'ffghfgh'),
(53, NULL, 'nnn', 'nnn'),
(54, NULL, 'nnn', 'nnn'),
(55, NULL, 'nnn', 'nnn'),
(56, 65, 'gfhfgh', 'ffghfgh'),
(57, NULL, 'nnn', 'nnn'),
(58, NULL, 'nnn', 'nnn'),
(59, NULL, 'nnn', 'nnn'),
(60, NULL, 'fff', 'fff'),
(61, NULL, 'fff', 'fff'),
(62, NULL, 'ggg', 'ggg'),
(63, NULL, 'ggg', 'ggg'),
(64, NULL, 'f', 'f'),
(65, NULL, 'j', 'j'),
(66, NULL, 'j', 'j'),
(67, NULL, 'hh', 'hh'),
(68, NULL, 'hh', 'hh'),
(69, NULL, 'g', 'g'),
(70, NULL, 'dfgdgf', 'dfg'),
(71, NULL, 'gg', 'gg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu "description"', 'Instalka CMS Lemonade Studio'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu "title"', 'StalTech'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'oleh@lemonadestudio.pl'),
(7, 'contact_phone', 'Telefon kontaktowy', '+48 661 965 646'),
(8, 'contact_email', 'E-Mail kontaktowy', 'biuro@staltech.pl'),
(9, 'copyright', 'Copyright', 'Wszelkie prawa zastrzeżone - StalTech - 2016'),
(10, 'contact_title', 'Kontakt - Nagłówek', 'StalTech Jacek Sieciechowicz'),
(11, 'contact_adress1', 'Kontakt - Adres 1', 'Łążek Ordynacki'),
(12, 'contact_adress2', 'Kontakt - Adres 2', '23-300 Janów Lubelski'),
(13, 'contact_nip', 'Kontakt - NIP', 'NIP: 862-157-55-05'),
(14, 'contact_woj', 'Kontakt - Województwo', 'woj. lubelskie'),
(15, 'google_maps_position', 'Koordynaty Google Maps X,Y (oddzielono przecinkiem)', '50.6985415,22.4157732'),
(16, 'limit_news', 'Liczba wiadomości na stronie "Aktualności"', '5'),
(17, 'limit_news_block', 'Liczba wiadomości w bloku "Aktualności" na podstronach', '2'),
(18, 'limit_realizations', 'Ilość realizacji wyświetlanych na jednej stronie listowania', '10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH","ROLE_USER"]', '2015-07-31 13:46:34', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_offer`
--
ALTER TABLE `contact_offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `realizations`
--
ALTER TABLE `realizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_1GG399504E7AF8G` (`gallery_id`);

--
-- Indexes for table `realizations_options`
--
ALTER TABLE `realizations_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `contact_offer`
--
ALTER TABLE `contact_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `realizations`
--
ALTER TABLE `realizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT dla tabeli `realizations_options`
--
ALTER TABLE `realizations_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
