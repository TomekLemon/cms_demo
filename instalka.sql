--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu "description"', 'Instalka CMS Lemonade Studio'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu "title"', 'Instalka'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'rafalglazar@gmail.com'),
(5, 'google_recaptcha_secret_key', 'Google reCAPTCHA - secret key', NULL),
(6, 'google_recaptcha_site_key', 'Google reCAPTCHA - site key', NULL);

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH","ROLE_USER"]', '2015-07-31 13:46:34', NULL);
